package be.coder.moon.hoofdstuk17;

import be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2.DrawingContext;
import be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2.NegativeSizeException;



/**
 *  A <code>Rectangle</code> is a Rectangle.
 * <h2>I am a rectangle</h2>
 * The Rectangle program implements an application that
 *  is a rectangle in a 2D area . .


  * @author  Moon
  * @version 1.0
  * @since  2021-01-22
 */
public class Rectangle extends Shape {


    public static final int ANGLES;
    private static int count;
    private int height;
    private int width;
    public final String description = "I am a rectangle";

    static {
        ANGLES = 4;
        count = 0;
    }

    {
        count++;
    }

    /** Creates a new Rectangle object with no args */
    public Rectangle() {
        this(0,0);
    }

    /**
     * Creates a new Rectangle object with specified height and width
     * @param height height
     * @param width width
     * set x and y as 0.
     */
    public Rectangle(int height, int width) {
        this(height,width,0,0);
    }

    /**
     * Creates a new Rectangle object with specified height, width,x and y
     * @param height height
     * @param width width
     * @param x x
     * @param y y
     */
    public Rectangle(int height, int width, int x, int y) {
        super(x, y);
        this.setHeight(height);
        this.setWidth(width);
    }

    /**
     * Creates a new Rectangle object with specified an other Rectangle object
     * @param rect
     */
    public Rectangle(Rectangle rect) {
        this(rect.getHeight(),  rect.getWidth(), rect.getX(), rect.getY());
    }



    /**
     * This method is used to access the value of private height.
     * @return int This returns value of height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * This method is used to set the value of height..
     * @param height This is the parameter to setHeight method
     * @return Nothing..
     * @throws NegativeSizeException in case of passing non-positive argument
     */
    public void setHeight(int height) {
       if(height<0){
            throw new NegativeSizeException();
        }
        this.height = height ;
    }

    /**
     * Gets width of the Rectangle instance.
     * @return int This returns value of width.
     */
    public int getWidth() {
        return width;
    }


    /**
     * This method is used to set the value of width..
     * @param width This is the parameter to setWidth method
     * @return Nothing..
     * @throws NegativeSizeException in case of passing non-positive argument
     */
    public void setWidth(int width) {
        if(width<0){
            throw new NegativeSizeException();
        }
        this.width = width ;
    }

    /**
     * Gets count of the instance number which initialized from Rectangle class.
     * @return int This returns value of count.
     */
    public static int getCount() {
        return count;
    }


    /**
     * This method is used to grow the size of the height and width..
     * @param d will be added to the height and width
     * @return Nothing..
     * @throws NegativeSizeException in case of passing an
     * argument which makes the result of (d+height) or (d+width) negative.
     */
    public void grow(int d){
        setHeight(getHeight()+d);
        setWidth(getWidth()+d);
    }

    @Override
    public double getArea(){
        return height * width;
    }

    @Override
    public double getPerimeter(){
        return 2 * (height + width);
    }

    /** Just print the values of the fields. */
    @Override
    public String toString() {
        return "Rectangle{" +
                "height=" + height +
                ", width=" + width +
                ", description='" + description + '\'' +
                '}';
    }



    @Override
    public void scale(int scale) {
        setHeight((getHeight()*scale) /100);
        setWidth((getWidth()*scale) /100);
    }

    @Override
    public void draw(DrawingContext drawingContext) {

    }
}
