package be.coder.moon.hoofdstuk6.opdracht1;

import java.util.Random;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class RandomApp {
    public static void main(String[] args) {

        Random random = new Random();

        int number1 = random.nextInt();
        System.out.println(number1);

        int number2 = random.nextInt(50);
        System.out.println(number2);


    }
}
