package be.coder.moon.hoofdstuk6.opdracht1;

import java.util.Random;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class LotteryApp {
    public static void main(String[] args) {

        Random random = new Random();

        for (int i = 0; i < 6; i++) {
          int lotteryNumber = random.nextInt(45);
            System.out.println(lotteryNumber);
        }
    }
}
