package be.coder.moon.hoofdstuk6.String;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class Opdracht3 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Write a text please :");
        String text = keyboard.nextLine();
        System.out.println(text.length());
        System.out.println(text.toUpperCase(Locale.ROOT));
        System.out.println(text.toLowerCase());
        String newText = text.replaceAll("a","o");
        System.out.println(newText);
        System.out.print("Write another text please :");
        String second = keyboard.nextLine();
        System.out.println(newText.equals(second));

        if(newText.compareTo(second) <  0){
            System.out.println("  first : " + newText);
            System.out.println("  second : " + second);
        }

        else {
            System.out.println("  first : " + second);
            System.out.println(" second : " + newText);
        }


        System.out.print("Write another text with spaces front and back, please :");
        String third = keyboard.nextLine();
        System.out.println(third.strip());


        keyboard.close();
    }
}
