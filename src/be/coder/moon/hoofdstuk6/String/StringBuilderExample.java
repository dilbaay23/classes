package be.coder.moon.hoofdstuk6.String;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class StringBuilderExample {
    public static void main(String[] args) {

        StringBuilder sb1 = new StringBuilder("Hello World !");
        StringBuilder sb2 = new StringBuilder("Hello Jupiter !");
        sb1.append(" And Hello Mars:)");
        System.out.println(sb1);

       // System.out.println(sb2.reverse());

        for (int i = 0; i < sb1.length(); i++) {
            if (sb1.charAt(i)==' ') sb1.replace(i,i+1,"");
        }
        System.out.println(sb1);

        for (int i = 0; i < sb2.length(); i++) {
            if (sb2.charAt(i)=='t') {
                sb2.replace(i, i + 1, "tt");
                i++;
            }
        }
        System.out.println(sb2);

    }

}
