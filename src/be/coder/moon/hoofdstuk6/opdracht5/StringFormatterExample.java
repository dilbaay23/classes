package be.coder.moon.hoofdstuk6.opdracht5;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class StringFormatterExample {
    public static final double METRES_TO_FEET_CONVERSATION = 3.281;
    public static void main(String[] args) {
        for (double i = 1; i < 20; i += 0.5) {

            System.out.printf("%5.2f metres = %5.2f feet%n",i,convertMToFt(i));
        }
    }

    private static double convertMToFt(double metres) {
        return  metres * METRES_TO_FEET_CONVERSATION;
    }

}
