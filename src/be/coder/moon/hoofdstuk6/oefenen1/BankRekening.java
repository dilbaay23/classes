package be.coder.moon.hoofdstuk6.oefenen1;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class BankRekening {
    private String rekeningNr;
    private double saldo;

    public BankRekening() {
    }

    public BankRekening(String rekeningNr, double saldo) {
        this.rekeningNr = rekeningNr;
        this.saldo = saldo;
    }

    public String getRekeningNr() {
        return rekeningNr;
    }

    public void setRekeningNr(String rekeningNr) {
        this.rekeningNr = rekeningNr;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
