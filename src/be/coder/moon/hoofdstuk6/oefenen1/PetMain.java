package be.coder.moon.hoofdstuk6.oefenen1;

import java.time.LocalDate;
import java.time.Month;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class PetMain {
    public static void main(String[] args) {
        Pet pet = new Pet();
        pet.setName("Mars");
        pet.setBreed("man");
        pet.setId(452L);
        pet.setBirthday(LocalDate.of(2020,Month.JULY,15));

        System.out.println(pet.getBirthday());


    }



}
