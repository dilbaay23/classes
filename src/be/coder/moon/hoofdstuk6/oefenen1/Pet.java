package be.coder.moon.hoofdstuk6.oefenen1;

import java.time.LocalDate;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class Pet {

    private Long id;
    private String name;
    private String type;
    private LocalDate birthday;
    private String breed;

    public Pet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }


    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
