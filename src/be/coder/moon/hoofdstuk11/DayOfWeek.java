package be.coder.moon.hoofdstuk11;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public enum DayOfWeek {
    SUNDAY(false),
    MONDAY(true),
    TUESDAY(true),
    WEDNESDAY(true),
    THURSDAY(true),
    FRIDAY(true),
    SATURDAY(false);

    private boolean isWeekDay;

    DayOfWeek(boolean isWeekDay) {
        this.isWeekDay = isWeekDay;
    }

    public boolean isWeekDay() {
        return isWeekDay;
    }

    @Override
    public String toString() {
        return name() + " is weekday:" + isWeekDay ;
    }
}

