package be.coder.moon.hoofdstuk15.opdarcht12345;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/22/2021, Fri
 **/
public class Division {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        try {
            int num = Integer.parseInt(scanner.next());
            int den = Integer.parseInt(scanner.next());
            int div = num /den;

            System.out.printf("%d / %d = %d\n", num,den,div);
            return;

        }catch (RuntimeException runtimeException){
            System.out.println("ERROR");
        }
        /* catch (NumberFormatException nfe) {
            System.out.println("You should entered an integer number... but too late !!!");
            System.out.println(nfe.getMessage());
            // nfe.printStackTrace();
        }catch(ArithmeticException ae){
            System.out.println("Division by 0 !!!");
        }*/
        finally{
            scanner.close();
            System.out.println("finally run...");
        }

        System.out.println("The End");



    }
}
