package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

/**
 * Created By Moon
 * 1/22/2021, Fri
 **/
public class Rectangle extends Shape {

    //Fields
    public static final int ANGLES;
    private static int count;
    private int height;
    private int width;
    public final String description = "I am a rectangle";

    static {
        ANGLES = 4;
        count = 0;
    }

    {
        count++;
    }
    public Rectangle() {
        this(0,0);
    }

    public Rectangle(int height, int width) {
        this(height,width,0,0);
    }


    public Rectangle(int height, int width, int x, int y) {
        super(x, y);
        this.setHeight(height);
        this.setWidth(width);
    }



    public Rectangle(Rectangle rect) {
        this(rect.getHeight(), rect.getWidth(), rect.getX(), rect.getY());
    }



    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
       if(height<0){
            throw new NegativeSizeException();
        }
        this.height = height ;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if(width<0){
            throw new NegativeSizeException();
        }
        this.width = width ;
    }

    public static int getCount() {
        return count;
    }




    public void grow(int d){
        this.height = d < 0?height:height+d;
        this.width = d < 0?width:width+d;
    }

    @Override
    public double getArea(){
        return height * width;
    }

    @Override
    public double getPerimeter(){
        return 2 * (height + width);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "height=" + height +
                ", width=" + width +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

    @Override
    public void scale(int scale) {
        setHeight((getHeight()*scale) /100);
        setWidth((getWidth()*scale) /100);
    }
}
