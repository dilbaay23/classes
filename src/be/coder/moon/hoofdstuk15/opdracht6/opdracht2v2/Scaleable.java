package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

public interface Scaleable {


    public  static final int DOUBLE = 200;
    public  static final int HALF = 50;
    public  static final int QUARTER = 25;

    void scale(int factor);
    default void scaleDouble(){
        scale(DOUBLE);
    }
    default void scaleHalf(){
        scale(HALF);
    }
}
