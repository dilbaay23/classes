package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public class DrawingContextApp {
    public static void main(String[] args) {

      try {
          Shape shape = new Rectangle(5, -4);
          System.out.println(shape.getArea());
        } catch (NegativeSizeException e) {
            System.out.println(e.getMessage());
        }



    }
}