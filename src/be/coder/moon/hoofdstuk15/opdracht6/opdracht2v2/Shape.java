package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

public abstract class Shape implements Drawable {
    private static int count = 0;
    private int x ;
    private int y;

    {
        count++;
    }

    public Shape() {
        this(0,0);
    }

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static int getCount() {
        return count;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    abstract double getArea();

    abstract double getPerimeter();


}
