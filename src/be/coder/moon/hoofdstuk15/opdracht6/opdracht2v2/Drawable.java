package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

public interface Drawable extends Scaleable {

    void draw(DrawingContext drawingContext);
}
