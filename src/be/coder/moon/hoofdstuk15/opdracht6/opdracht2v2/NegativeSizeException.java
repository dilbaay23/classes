package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

/**
 * Created By Moon
 * 1/22/2021, Fri
 **/
public class NegativeSizeException  extends  IllegalArgumentException{

    private final static String MESSAGE = "A length can not be negative..";

    public NegativeSizeException(){
        super(MESSAGE);
    }

}
