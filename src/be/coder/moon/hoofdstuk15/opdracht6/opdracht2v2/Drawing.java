package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

import java.util.Arrays;

public class Drawing implements Drawable {

    private  Drawable[] drawables;

    private  int size ;

    public void setSize(int size) {
        this.size = size;
    }

    public Drawing() {
        drawables = new Drawable[100];
    }

    public Drawable[] getDrawables() {
        return drawables;
    }

    public void add(Drawable drawable){
        int index = getFreeLocation();

            if (!isPresent(drawable) && index != -1) {
                drawables[index] = drawable;
                size++;
            }

    }

    private boolean isPresent(Drawable drawable) {

        for (Drawable drawable1 : drawables) {
            if (drawable1 != null && drawable1.equals(drawable))
                return true;
        }
        return false;


    }


    private int getFreeLocation() {

        for (int i = 0; i < drawables.length; i++) {
            if (drawables[i] == null)
                return i;
        }
        return  -1;
    }

    public void remove(Drawable drawable){
        for (int i = 0; i < drawables.length; i++) {
            if(drawables[i] != null && drawables[i].equals(drawable)){
                drawables[i] = null;
                size--;
                break;
            }

        }
    }

    public void clear(){
        Arrays.fill(drawables, null);
        size = 0;

    }


    public int getSize(){
        return size;
    }




    public String print() {
        String result="";
        for (Drawable drawable : drawables) {
           result=  drawable.toString();
        }
        return result;
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        for (Drawable drawable : drawables) {
            if (drawable != null)
            drawable.draw(drawingContext);
        }
    }

    @Override
    public void scale(int factor) {
        for (Drawable drawable : drawables) {
            if (drawable != null)
            drawable.scale(factor);
        }
    }
}
