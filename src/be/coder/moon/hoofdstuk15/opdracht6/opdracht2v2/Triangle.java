package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public class Triangle extends Shape {
    //Fields
    public static final int ANGLES = 3;
    private static int count;
    private int height;
    private int width;
    private int perpendicular;

    {
        count++;
    }

    public Triangle() {
        this(0, 0, 0);
    }

    public Triangle(int height, int width, int perpendicular) {

        this(0, 0, height, width, perpendicular);
    }

    public Triangle(int x, int y, int height, int width, int perpendicular) {
        super(x, y);
        setHeight(height);
        setWidth(width);
        setPerpendicular(perpendicular);

    }

    public Triangle(Triangle otherTriangle) {
        if (otherTriangle != null) {
            this.setX(otherTriangle.getX());
            this.setY(otherTriangle.getY());
            this.width = otherTriangle.getWidth();
            this.height = otherTriangle.getHeight();
        }

    }

    public static int getCount() {
        return count;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = Math.abs(height);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = Math.abs(width);
    }

    public int getPerpendicular() {
        return perpendicular;
    }

    public void setPerpendicular(int perpendicular) {
        this.perpendicular = Math.abs(perpendicular);
    }

    @Override
    public double getArea() {
        return (getPerpendicular() * getHeight()) / 2;
    }

    //TODO : ASK for perpendicular
    @Override
    public double getPerimeter() {
        return 0;
    }


    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

    @Override
    public void scale(int scale) {

        setHeight((getHeight()*scale) /100);
        setWidth((getWidth()*scale) /100);
        setPerpendicular((getPerpendicular()*scale) /100);
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "height=" + height +
                ", width=" + width +
                ", perpendicular=" + perpendicular +
                '}';
    }
}
