package be.coder.moon.hoofdstuk15.opdracht6.opdracht2v2;

public class IsoScelesTriangle extends Triangle {

    private static int count = 0;

    {
        count++;
    }

    public IsoScelesTriangle() {
     super(0,0,0,0,0);
    }

    public IsoScelesTriangle(int height, int width) {
        super(0,0,height,width,0);
    }

    public IsoScelesTriangle(int x, int y, int height, int width) {
        super(0,0,0,0,0);
    }

    public IsoScelesTriangle(IsoScelesTriangle triangle) {

        super(triangle);
    }

    public static int getCount() {
        return count;
    }

    public void setWidth(int width){
        setWidth(width);
    }

    public void setPerpendicular(int p){
        setPerpendicular(p);
    }


}
