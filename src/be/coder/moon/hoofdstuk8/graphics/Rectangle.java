package be.coder.moon.hoofdstuk8.graphics;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class Rectangle {
    public static final int ANGELS ;
    private static int NUMBER_OF_OBJECT;
    private int x;
    private int y;
    private int width ;
    private int height;
    public final String  description = "I am a rectangle ";

    {
        NUMBER_OF_OBJECT++;
    }
    static {
        ANGELS = 4;
    }

    public Rectangle() {
        this(1,1);
    }

    public Rectangle(int width, int height) {
        this(0,0,width,height);
    }

    public Rectangle(int x, int y, int width, int height) {
        setWidth(width);
        setHeight(height);
        setX(x);
        setY(y);
    }

    public Rectangle(Rectangle otherRectangle){
        if(otherRectangle != null) {
            this.x = otherRectangle.getX();
            this.y = otherRectangle.getY();
            this.width = otherRectangle.getWidth();
            this.height = otherRectangle.getHeight();
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getDescription() {
        return description;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setWidth(int width) {
        this.width = Math.abs(width);
    }

    public void setHeight(int height) {
        this.height = Math.abs(height);
    }

    public void setPosition(int x, int y){
        setX(x);
        setY(y);
    }

    public void grow(int d){
        setHeight(getHeight()+d);
        setWidth(getWidth()+d);

    }

    public double getArea(){
        return  getHeight() * getWidth();
    }

    public double getPerimeter(){
        return  2 * (getHeight() + getWidth());
    }

    public static int getNumberOfObject() {
        return NUMBER_OF_OBJECT;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
