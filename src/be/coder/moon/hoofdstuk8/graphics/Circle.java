package be.coder.moon.hoofdstuk8.graphics;

import static java.lang.Math.PI;

/**
 * Created By Moon
 * 1/14/2021, Thu
 **/
public class Circle {
    public static final int ANGELS ;
    private static int count;
    private int x;
    private int y;
    private int radius;

    static {
        ANGELS = 0;
    }

    {
        count++;
    }

    public Circle() {
        this(0);
    }

    public Circle(int radius) {
        this(0,0,radius);
    }

    public Circle(int x, int y, int radius) {
       setY(x);
       setY(y);
       setRadius(radius);
    }
    public Circle(Circle circle) {
        this(circle.getX(),circle.getY(),circle.getRadius());
    }

    public static int getCount() {
        return count;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius < 0 ? -radius : radius;
    }
    public void setPosition(int x, int y){
        setX(x);
        setY(y);
    }

    public void grow(int d){
        setRadius(getRadius()+d);
    }

    public double getArea(){
        return  PI * getRadius() * getRadius();
    }

    public double getPerimeter(){
        return  2 * PI * getRadius();
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                '}';
    }
}
