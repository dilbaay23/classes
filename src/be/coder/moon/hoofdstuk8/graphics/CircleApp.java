package be.coder.moon.hoofdstuk8.graphics;

import static be.coder.moon.hoofdstuk8.graphics.Circle.*;
import static be.coder.moon.hoofdstuk8.graphics.Rectangle.getNumberOfObject;

/**
 * Created By Moon
 * 1/14/2021, Thu
 **/
public class CircleApp {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        printCircle(c1);
        Circle c2 = new Circle(8);
        printCircle(c2);
        Circle c3 = new Circle(1,2,5);
        printCircle(c3);
        Circle c4 = new Circle(c3);
        printCircle(c4);

    }
    public static void printCircle(Circle circle){
        System.out.println("Circle:" + "\n\tRadius : " + circle.getRadius()

                + "\n\tx : " + circle.getX()
                + "\n\tY : " + circle.getY()
                + "\n\tArea : " + circle.getArea()
                + "\n\tPerimeter : " + circle.getPerimeter()
                +"\n\tAngles of the circle : "+ ANGELS
                +"\n\tObject number of the Circle class is : "+ getCount());
    }
}
