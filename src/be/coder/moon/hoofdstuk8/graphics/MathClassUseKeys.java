package be.coder.moon.hoofdstuk8.graphics;

import static java.lang.Math.*;

/**
 * Created By Moon
 * 1/14/2021, Thu
 **/

public class MathClassUseKeys {
    public static void main(String[] args) {
        for (double i = 0; i <= 2 * PI; i += 0.1) {
            System.out.println(cos( i * 180 / PI ));
            System.out.println(toDegrees(i));
        }

    }
}
