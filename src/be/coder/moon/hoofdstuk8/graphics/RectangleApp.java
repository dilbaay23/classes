package be.coder.moon.hoofdstuk8.graphics;

import static be.coder.moon.hoofdstuk8.graphics.Rectangle.getNumberOfObject;

/**
 * Created By Moon
 * 1/12/2021, Tue
 **/
public class RectangleApp {
    public static void main(String[] args) {
        System.out.println("This program uses a rectangle ");
        Rectangle rectangle1 = new Rectangle();
        printRectangle(rectangle1);
        Rectangle rectangle2 = new Rectangle(-5,2);
        printRectangle(rectangle2);
        Rectangle rectangle3 = new Rectangle(1,1,4,-3);
        printRectangle(rectangle3);
        Rectangle rectangle4 = new Rectangle(rectangle3);
        printRectangle(rectangle4);
    }

    public static void printRectangle(Rectangle rectangle){
        System.out.println("Rectangle:" + "\n\tHeight : " + rectangle.getHeight()
                                        + "\n\tWidth : " + rectangle.getWidth()
                                        + "\n\tx : " + rectangle.getX()
                                        + "\n\tY : " + rectangle.getY()
                                        +"\n\tAngles of the rectangle : "+ Rectangle.ANGELS
                                        +"\n\tObject number of the Rectangle class is : "+ getNumberOfObject());
    }
}
