package be.coder.moon.hoofdstuk10;

import be.coder.moon.hoofdstuk8.graphics.Rectangle;

import static be.coder.moon.hoofdstuk8.graphics.Rectangle.getNumberOfObject;

/**
 * Created By Moon
 * 1/18/2021, Mon
 **/
public class SquareApp {
    public static void main(String[] args) {
        Rectangle square = new Square(4);
//        square.setSide(7);
//        square.setHeight(11);
        square.setPosition(2,2);
        printSquare(square);

        Square square2 = new Square(4,4,12);
      //  square2.setSide(-17);
      //  square2.setPosition(2,2);
        printSquare(square2);

        Rectangle square3 = new Square(square2);
        printSquare(square3);

    }
    public static void printSquare(Rectangle square){
        System.out.println("Square:" + "\n\tDescription : " + square.getDescription()
             //   + "\n\tSide : " + square.getSide()
                + "\n\tx : " + square.getX()
                + "\n\tY : " + square.getY()
                + "\n\tWidth : " + square.getWidth()
                + "\n\tHeight : " + square.getHeight()
                + "\n\tArea : " + square.getArea()
                + "\n\tPerimeter : " + square.getPerimeter()
                +"\n\tAngles of the square : "+ Square.ANGELS
                +"\n\tObject number of the Square class is : "+ Square.getNumberOfObject());
    }
}
