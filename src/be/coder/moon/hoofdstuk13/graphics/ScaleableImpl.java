package be.coder.moon.hoofdstuk13.graphics;


/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public class ScaleableImpl {
    public static void main(String[] args) {
        Scaleable scaleable = new Scaleable() {
            @Override
            public void scale(int factor) {
                if (factor == DOUBLE) System.out.println("factor is Double: " + DOUBLE);
                else if(factor == HALF) System.out.println("factor is Half: " + HALF);
                else System.out.println("factor is Quarter: " + QUARTER);
            }
        };
        scaleable.scale(200);
    }
}
