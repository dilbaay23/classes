package be.coder.moon.hoofdstuk13.graphics;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public interface Scaleable {

    public  static final int DOUBLE = 200;
    public  static final int HALF = 50;
    public  static final int QUARTER = 25;

    void scale(int factor);
}
