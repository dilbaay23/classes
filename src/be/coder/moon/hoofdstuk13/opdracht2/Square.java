package be.coder.moon.hoofdstuk13.opdracht2;



/**
 * Created By Moon
 * 1/18/2021, Mon
 **/
public class Square extends Rectangle {
    private static int NUMBER_OF_OBJECT;
    public final String description = super.description +"(square)";
    {
        NUMBER_OF_OBJECT++;
    }

    public static int getNumberOfObject() {
        return NUMBER_OF_OBJECT;
    }

    public Square(){
        super();   // this(0) can be used too.
    }

    public Square(int side){
        super(side,side);
    }
    public Square(int x, int y, int side){
        super(x,y,side,side);
    }
    public Square(Rectangle rect){
        super(rect);
    }


    public int getSide() {
        return getWidth();
    }

    public void setSide(int side) {
        super.setWidth(side);
        super.setHeight(side);

    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    public String getDescription() {
        return description;
    }

    @Override
    public double getArea() {
        return getSide() * getSide();
    }

    @Override
    public double getPerimeter() {
        return getSide() * ANGELS;
    }
}
