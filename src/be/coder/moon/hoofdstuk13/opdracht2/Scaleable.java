package be.coder.moon.hoofdstuk13.opdracht2;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public interface Scaleable {

    public  static final int DOUBLE = 200;
    public  static final int HALF = 50;
    public  static final int QUARTER = 25;

    void scale(int factor);
    default void scaleDouble(){
        scale(DOUBLE);
    }
    default void scaleHalf(){
        scale(HALF);
    }
}
