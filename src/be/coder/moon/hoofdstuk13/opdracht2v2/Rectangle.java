package be.coder.moon.hoofdstuk13.opdracht2v2;

public class Rectangle extends Shape {


    public static final int ANGLES;
    private static int count;
    private int height;
    private int width;
    public final String description = "I am a rectangle";

    static {
        ANGLES = 4;
        count = 0;
    }

    {
        count++;
    }


    public Rectangle() {
        this(0,0);
    }

    public Rectangle(int height, int width, int x, int y) {
        super(x, y);
        this.setHeight(height);
        this.setWidth(width);
    }

    public Rectangle(int height, int width) {
        this.setHeight(height);
        this.setWidth(width);
        setX(0);
        setY(0);
    }

    public Rectangle(Rectangle rect) {
        this.setHeight(rect.height);
        this.setWidth(rect.width);
        setX(rect.getX());
        setY(rect.getY());
    }



    //Getter And Setters

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height < 0?-height:height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width < 0?-width:width;
    }

    public static int getCount() {
        return count;
    }

    //Business methods


    public void grow(int d){
        setHeight(getHeight()+d);
        setWidth(getWidth()+d);
    }

    @Override
    public double getArea(){
        return height * width;
    }

    @Override
    public double getPerimeter(){
        return 2 * (height + width);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "height=" + height +
                ", width=" + width +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public void draw(DrawingContext drawingContext) {

       drawingContext.draw(this);
       // System.out.println("Rectangle draws sth");
    }

    @Override
    public void scale(int scale) {
        setHeight((getHeight() * scale) /100);
        setWidth((getWidth() * scale) /100);
    }
}
