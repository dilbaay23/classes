package be.coder.moon.hoofdstuk13.opdracht2v2;

public class TextDrawingContext implements DrawingContext {

    @Override
    public void draw(Rectangle rectangle) {
        System.out.println(rectangle.toString());
    }

    @Override
    public void draw(Circle circle) {
        System.out.println(circle.toString());
    }

    @Override
    public void draw(Triangle triangle) {
        System.out.println(triangle.toString());
    }


}
