package be.coder.moon.hoofdstuk13.opdracht2v2;

import java.io.Serializable;

public interface Drawable extends Scaleable {

    void draw(DrawingContext drawingContext);
}
