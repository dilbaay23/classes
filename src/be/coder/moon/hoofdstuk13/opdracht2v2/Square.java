package be.coder.moon.hoofdstuk13.opdracht2v2;

public class Square extends Rectangle {
    private static int count = 0;
    public final String description = super.description + "(square)";
    public int side;

    {
        count++;
    }

    public Square() {
        this(0);
    }

    public Square(int side){
        this(side,0,0);
    }

    public Square(int side, int x, int y){
        super(side,side,x,y);
    }

    public Square(Square square){
        setSide(square.getSide());
    }

    public String getDescription() {
        return description;
    }

    public int getSide() {
        return getWidth();
    }

    public void setSide(int side) {
        super.setHeight(side);
        super.setWidth(side);
    }

    public static int getCount() {
        return count;
    }

    @Override
    public void setHeight(int height) {
        setSide(height);
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    @Override
    public double getArea() {
        return getSide() * getSide();
    }

    @Override
    public double getPerimeter() {
        return getSide() * ANGLES;
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        super.draw(drawingContext);
    }

    @Override
    public void scale(int scale) {
        setSide(getSide()*scale /100);
    }

    @Override
    public String toString() {
        return "Square{" +
                "description='" + description + '\'' +
                ", side=" + getWidth() +
                '}';
    }
}
