package be.coder.moon.hoofdstuk13.opdracht2v2;

public class IsoScelesTriangle extends Triangle {

    private static int count = 0;

    {
        count++;
    }

    public IsoScelesTriangle() {
     super(0,0,0,0,0);
    }

    public IsoScelesTriangle(int height, int width) {
        super(0,0,height,width,width/2);
    }

    public IsoScelesTriangle(int x, int y, int height, int width) {
        super(x,y,height,width,(width/2));
    }

    public IsoScelesTriangle(IsoScelesTriangle triangle) {

        super(triangle);
    }

    public static int getCount() {
        return count;
    }

    public void setWidth(int width){
        super.setWidth(width);
    }

    public void setPerpendicular(int p){
        super.setPerpendicular(p);
        setWidth(2*p);
    }


}
