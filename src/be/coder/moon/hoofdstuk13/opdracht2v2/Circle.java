package be.coder.moon.hoofdstuk13.opdracht2v2;

import static java.lang.Math.PI;

public class Circle extends Shape {

    //Fields
    public static final int ANGLES;
    private static int count;
    private int radius;

    static {
        ANGLES = 0;
        count = 0;
        count++;
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        setRadius(radius);
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public Circle(Circle otherCircle){

    }

    public Circle() {
    }

    public static int getCount() {
        return count;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = Math.abs(radius);
    }

    @Override
    public double getArea(){
        return PI * radius * radius;
    }

    @Override
    public double getPerimeter(){
        return 2 * PI * radius;
    }

    public void grow(int d){
      setX(getX() + d);
      setY(getY() + d);
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

    @Override
    public void scale(int scale) {
        setRadius((getRadius()*scale) /100);

    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
