package be.coder.moon.hoofdstuk13.opdracht2v2;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public class DrawingContextApp {
    public static void main(String[] args) {

     Drawable drawable = new Circle(5);
        Drawable drawable1 = new Rectangle(5, 4);
        Drawable drawable2 = new Square(6);
        Drawable drawable3 = new Circle(11);
        Drawable drawable4 = new Triangle(4, 6, 10);

        Drawing drawing = new Drawing();

        DrawingContext drawingContext = new TextDrawingContext();


        System.out.println(drawing.getSize());

        drawing.add(drawable1);
        drawing.add(drawable);

        System.out.println(drawing.getSize());

        drawing.add(drawable2);
        drawing.add(drawable3);
        drawing.add(drawable4);

        System.out.println(drawing.getSize());

        drawing.remove(drawable3);
        drawing.remove(drawable3);

        System.out.println(drawing.getSize());

        drawing.draw(drawingContext);

        drawing.scale(200);
        System.out.println("---------------");
        drawing.draw(drawingContext);

       Scaleable scaleable1 = new Rectangle(2,4,1,1);
       Scaleable scaleable2 = new Square(5,4,1);
       Scaleable scaleable3 = new Circle(6);

       scaleable1.scaleHalf();
       scaleable2.scaleDouble();
       scaleable3.scale(25);

        System.out.println("---------------");
        System.out.println(scaleable1.toString());
        System.out.println(scaleable2.toString());
        System.out.println(scaleable3.toString());


        // Test for Triangle classes and works right
        System.out.println("---------------");

        Shape shape = new Triangle(3,8,4);
        Triangle isoScelesTriangle = new IsoScelesTriangle(8,12);
        System.out.println(isoScelesTriangle);
        System.out.println(isoScelesTriangle.getArea());
        System.out.println(isoScelesTriangle.getPerimeter());
        System.out.println(isoScelesTriangle.getPerpendicular());

    }
}