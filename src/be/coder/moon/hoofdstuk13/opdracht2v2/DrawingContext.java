package be.coder.moon.hoofdstuk13.opdracht2v2;

public interface DrawingContext {

    void draw(Rectangle rectangle);
    void draw(Circle circle);
    void draw(Triangle triangle);


}
