package be.coder.moon.hoofdstuk12;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created By Moon
 * 1/21/2021, Thu
 **/
public class DateTimeDemo {
    public static void main(String[] args) {
        Instant now = Instant.now();
        System.out.println(now);
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
    }

}
