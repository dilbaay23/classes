package be.coder.moon.hoofdstuk7;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class SplitArray {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please write a text : ");
        String text = keyboard.nextLine();
        String[] words = text.split(" ");
        for (String word: words ) {
            System.out.println(word);
        }
        keyboard.close();
    }
}
