package be.coder.moon.hoofdstuk7;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class TwoDimensionalArray {
    public static void main(String[] args) {
        int[][] array = new int[4][6];

        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                array[row][col] = row * col;
            }
        }
        for (int[] arr :array) {
            for (int a: arr) {
                System.out.print(a + "\t");
            }
            System.out.println();
        }
    }
}
