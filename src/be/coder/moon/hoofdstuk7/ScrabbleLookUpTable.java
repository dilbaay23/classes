package be.coder.moon.hoofdstuk7;

import java.util.Scanner;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class ScrabbleLookUpTable {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please give a word :  ");

        String word = keyboard.next().toUpperCase();

        int[] scrabbleArray = {1, 3, 3, 2, 1, 4,
                2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10,
                1, 1, 1, 1, 4, 4, 8, 4, 10};

        int score = 0;

        for (int i = 0; i < word.length(); i++) {

            char calculatedLetter = word.charAt(i);   // aba

           score += scrabbleArray[calculatedLetter - 'A'];  //65  scrabbleArray[0] score=4+1

            }

        System.out.println("The score for the " + word  + " is : " + score);

        keyboard.close();

    }
}


































