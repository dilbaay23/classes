package be.coder.moon.hoofdstuk7;

import java.util.Arrays;

/**
 * Created By Moon
 * 1/14/2021, Thu
 **/
public class Arrays1 {
    public static void main(String[] args) {
/*
        boolean[] booleanArray = {true,false,false,true,true};
        for (boolean b:booleanArray) {
            System.out.println(b);
        }*/


        /*  int[] array = new int[20];

      for (int i = 0; i < array.length; i++) {
            array[i] = 7 * (i+1);
        }

        for (int a:array) {
            System.out.println(a);
        }
        for (int i = array.length-1; i >=0 ; i--) {
            System.out.println(array[i]);
        }*/


   //     Arrays.stream(array).sorted().forEach(System.out::println);
        int[] newArray = {5,10,5,7,14,89,3,74,15,66,22,0,47};
//         Arrays.sort(newArray);

//        for (int a:newArray) {
//            System.out.println(a);
//        }
//      Arrays.stream(newArray).sorted().forEach(System.out::println);

        int length=newArray.length;       //   5,10,5,7,14,89
        int temp;
        for(int i=0; i < length; i++){

            for(int j=1; j < (length-i); j++){

                if(newArray[j-1] > newArray[j]){
                    temp = newArray[j-1];
                    newArray[j-1] = newArray[j];
                    newArray[j] = temp;
                }

            }
        }


      /*  int lack ;
        for (int i = 0; i < newArray.length; i++) {
            for (int j = 0; j < i; j++) {
                if(newArray[i]<newArray[j]){
                    lack=newArray[j];
                    newArray[j]=newArray[i];
                    newArray[i]=lack;
                }
            }

        }*/
        for (int a:newArray) {
            System.out.println(a);
        }

    }
}
