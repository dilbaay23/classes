package be.coder.moon.hoofdstuk7;

/**
 * Created By Moon
 * 1/15/2021, Fri
 **/
public class Statistics {
    public static void main(String[] args) {

        int[] array = {3,5,9,7,11,5,6,12};

        System.out.println(average(array));
        System.out.println(min(array));
        System.out.println(max(array));
    }

    public static int average(int[] values){
        int count = values.length;
        int total = 0;
        for (int value:values) {
            total +=value;
        }
        return total/count;
    }

    public static int min(int[] values){
        int min = values[0];
        for (int value:
             values) {
            if(min>value) min=value;
        }
        return  min;
    }

    public static int max(int[] values){
        int max = values[0];
        for (int value:
                values) {
            if(max < value) max = value;
        }
        return  max;
    }
}
