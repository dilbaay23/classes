package be.coder.moon.hoofdstuk9;

import be.coder.moon.hoofdstuk8.graphics.Circle;
import be.coder.moon.hoofdstuk8.graphics.Rectangle;

/**
 * Created By Moon
 * 1/18/2021, Mon
 **/
public class LogoApp {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(3,5);
        Circle circle = new Circle(2);
        String text = "Hello my Logo";
        Logo logo = new Logo(rectangle,circle,text);
        System.out.println(logo);
    }
}
