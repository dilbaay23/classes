package be.coder.moon.hoofdstuk9;

import be.coder.moon.hoofdstuk8.graphics.Circle;
import be.coder.moon.hoofdstuk8.graphics.Rectangle;

/**
 * Created By Moon
 * 1/18/2021, Mon
 **/

public class Logo {
    private  Rectangle rectangle;
    private Circle circle;
    private String text;

    public Logo(Rectangle rectangle, Circle circle, String text) {
        this.rectangle = rectangle;
        this.circle = circle;
        this.text = text;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double totalArea(){
       return rectangle.getArea()+ circle.getArea();
    }

    @Override
    public String toString() {
        return "Logo{" +
                "rectangle=" + rectangle +
                ", circle=" + circle +
                ", text='" + text + '\'' +
                '}';
    }
}
